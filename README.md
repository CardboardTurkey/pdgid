# pdgid

A command-line tool to quickly get a particle's name from its PDGID and vice versa.

All elementary SM particles are included as well as some (hopefully) relevant
hadrons and BSM particles. The data was sourced from a combination of
[the PDG 2020 review](https://pdg.lbl.gov/2020/reviews/rpp2020-rev-monte-carlo-numbering.pdf),
[the CERN twiki](https://twiki.cern.ch/twiki/bin/view/Main/PdgId), and
[rivet](https://gitlab.com/hepcedar/rivet/-/blob/release-3-1-x/include/Rivet/Tools/ParticleName.hh).

If you want additional particles added, extra features or find a bug, please
submit an MR or create a issue! I am waiting to see if this package gains traction.

## Installation

```bash
pip install pdgid
```

`pdgid` has no dependencies so you are free to install it outside of a virtual
environment and not worry about package conflicts.

Requires `python3.6` or higher.

## Usage

Pass the command an id to get the associated particle name:
```bash
> pdgid 14
ν_μ
```
Or pass a particle name to get the associated id:
```bash
> pdgid ν_μ
14 [ν_μ]
```

Note the particle name can often be written in many forms and unicode is
accepted (e.g. "nu_mu", "Vμ", "ν_μ", "muon neutrino", etc...). The name that's been matched is displayed in the square brackets to help with cases of ambiguity.

Finally, when ran without arguments the tool will print a table of all the particles it
knows (along with their id).

## Limitations

Searching by name doesn't support using "bar" to represent antiparticle.

Order matters for particles with multiple super/subscripts. This could be
fixed with even more regex.

## License
MIT
