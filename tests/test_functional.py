"""Functional tests for pdgid"""

import subprocess
import pytest


@pytest.mark.parametrize(
    "args,expected_output",
    [
        ("u", "2 [up]"),
        ("u-quark", "2 [up]"),
        ("up", "2 [up]"),
        ("up-quark", "2 [up]"),
        ("up-quarks", "2 [up]"),
        ("s", "3 [strange]"),
        ("s-quark", "3 [strange]"),
        ("strange", "3 [strange]"),
        ("strange-quark", "3 [strange]"),
        ("strange-quarks", "3 [strange]"),
        ("t'", "8 [t']"),
        ("top'", "8 [t']"),
        ("truth'", "8 [t']"),
        ("truth-prime", "8 [t']"),
        ("top prime-quark", "8 [t']"),
        ("truth-prime quark", "8 [t']"),
        ("numu", "14 [ν_μ]"),
        ("ν_μ", "14 [ν_μ]"),
        ("9", "gluon (also 21)"),
        ("g", "21 or 9 [gluon]"),
        ("G", "39 [graviton]"),
        ("gravitons", "39 [graviton]"),
        ("W", "24 [W⁺]"),
        ("W+", "24 [W⁺]"),
        ("W^+", "24 [W⁺]"),
        ("W-boson", "24 [W⁺]"),
        ("W^+ boson", "24 [W⁺]"),
        ("W^+-bosons", "24 [W⁺]"),
        ("W-", "-24 [W⁻]"),
        ("W^--bosons", "-24 [W⁻]"),
        ("higgs", "25 [h⁰ / H₁⁰]"),
        ("z-prime", "32 [Z' / Z₂⁰]"),
        ("ZPrime", "32 [Z' / Z₂⁰]"),
        ("z'", "32 [Z' / Z₂⁰]"),
        ("z_2^0", "32 [Z' / Z₂⁰]"),
        ("w+2", "34 [W' / W₂⁺]"),
        ("w_2^+", "34 [W' / W₂⁺]"),
        ("w_2^-", "-34 [W' / W₂⁻]"),
        ("A0", "36 [A⁰ / H₃⁰]"),
        ("h++", "38 [H⁺⁺]"),
        ("h^-^-", "-38 [H⁻⁻]"),
        ("a0", "40 [a⁰ / H₄⁰]"),
        ("pi", "111 [π⁰]"),
        ("pion", "111 [π⁰]"),
        ("pion0", "111 [π⁰]"),
        ("pion^0", "111 [π⁰]"),
        ("K0S", "310 [K⁰ short]"),
        ("K^0_S", "310 [K⁰ short]"),
        ("phi", "333 [φ]"),
        ("J/psi", "443 [J/ψ]"),
        ("psion", "443 [J/ψ]"),
        ("D*^-", "-413 [D*⁻]"),
        ("D*+", "413 [D*⁺]"),
        ("D", "421 [D⁰]"),
        ("upsilon2s", "100553 [ϒ(2S)]"),
        ("ϒ(4s)", "300553 [ϒ(4S)]"),
        ("charm b+", "541 [charmed B⁺]"),
        ("b-c", "-541 [charmed B⁻]"),
        ("b-_c", "-541 [charmed B⁻]"),
        pytest.param(
            "omega_c^-", "4332 [charmed Ω⁻]", marks=pytest.mark.xfail
        ),  # Order matters :(
        ("Aluminium", "1000130270 [Aluminium]"),
    ],
)
def test_valid_input(args, expected_output):
    """Check that pdgid prints the correct output for various valid particle IDs and names"""
    output = subprocess.run(
        ["pdgid", args],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=True,
    )
    stdout, stderr = output.stdout.decode("UTF-8"), output.stderr.decode("UTF-8")
    assert stdout == f"{expected_output}\n", "pdgid didn't give the right answer"
    assert stderr == "", "Call to pdgid gave error message"


def test_no_input():
    """Check that pdgid at least tries to print a table when no input is provided"""
    output = subprocess.run(
        ["pdgid"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True
    )
    stdout, stderr = output.stdout.decode("UTF-8"), output.stderr.decode("UTF-8")
    data = [line for line in stdout.split("\n") if line.strip() != ""]
    for line in data:
        assert int(line.split(":")[0]), "Printed id that wasn't int"
        assert str(line.split(":")[1]), "Didn't print particle name"
    assert stderr == "", "Call to pdgid gave error message"


@pytest.mark.parametrize(
    "args,expected_output",
    [
        ("10", "ERROR - Unknown PDGID: 10"),
        ("harambe", "ERROR - Unknown particle name: harambe"),
        ("strange-", "ERROR - Unknown particle name: strange-"),
        ("W^", "ERROR - Unknown particle name: W^"),
        # Regex in particle display name
        pytest.param(
            "DDDDD⁺",
            "ERROR - Unknown particle name: DDDDD⁺",
            marks=pytest.mark.xfail,
        ),
    ],
)
def test_invalid_input(args, expected_output):
    """Check that pdgid prints the correct output for various *in*valid particle IDs and names"""
    output = subprocess.run(
        ["pdgid", args],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=True,
    )
    stdout, stderr = output.stdout.decode("UTF-8"), output.stderr.decode("UTF-8")
    assert stdout == "", "Call to pdgid gave output message"
    assert stderr == f"{expected_output}\n", "pdgid didn't give the right answer"
