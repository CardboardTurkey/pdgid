#!/usr/bin/env bash
black --check --exclude=.venv ./
pylint pdgid tests 
mypy pdgid/pdgid.py
