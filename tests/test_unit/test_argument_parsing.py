"""Functions to test that `pdgid.parse_args` behaves as expected"""

import pytest
from pdgid import pdgid


def test_no_arguments():
    """Test behaviour when no command-line arguments are presented"""
    args = pdgid.parse_args([])
    assert (
        args.particle == ""
    ), "Argument-less call to `parse_args` failed to leave `particle` empty"


@pytest.mark.parametrize("argument", ("gluon", "10"))
def test_one_argument(argument):
    """Test behaviour when one command-line argument is presented"""
    args = pdgid.parse_args([argument])
    assert args.particle == argument, f"Passing '{argument}' to `parse_args` failed"


def test_multiple_arguments():
    """Test behaviour when multiple command-line arguments are presented"""
    args = pdgid.parse_args(["z", "boson"])
    assert args.particle == "z boson", "`parse_args` failed to merge arguments"
