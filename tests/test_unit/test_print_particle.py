"""Test that `pdgid.print_particle` behaves as expected"""

import contextlib
import io
from collections import namedtuple

import pytest
from pdgid import pdgid


# Use this to simulate pdgid's argparse object
Arguments = namedtuple("Arguments", "particle")


@pytest.mark.parametrize(
    "args,expected_output",
    [(Arguments("9"), "g\n"), (Arguments("higgs"), "25 [higgs]\n")],
)
def test_print_particle(args, expected_output):
    """Check that the pdgid.print_particle prints the output we expect for '9' and 'higgs'"""
    text_stream = io.StringIO()
    with contextlib.redirect_stdout(text_stream):
        pdgid.print_particle(args, {9: "g", 25: "higgs"}, {"higgs": 25})
    output = text_stream.getvalue()
    assert output == expected_output
