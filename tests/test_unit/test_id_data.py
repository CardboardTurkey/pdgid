"""Functions to test that `pdgid.get_id_data` behaves as expected"""

from pdgid import pdgid


def test_id_to_name():
    """Test that `pdgid` successfully loaded a dict (called id_to_name) from the yaml.
    Check it has the right typings"""
    id_to_name, _ = pdgid.get_id_data()
    assert isinstance(
        id_to_name, dict
    ), "Somehow yaml file wasn't loaded as a dictionary"
    for id_, names in id_to_name.items():
        assert isinstance(id_, int), f"'{id_}' should be an integer"
        for name in names:
            assert isinstance(name, str), f"'{name}' should be an string"


def test_name_to_id():
    """Test that `pdgid` successfully inverted the dict mentioned above"""
    _, name_to_id = pdgid.get_id_data()
    assert isinstance(name_to_id, dict), "Failed to invert yaml as a dictionary"
    for name, id_ in name_to_id.items():
        assert isinstance(name, str), f"'{name}' should be a string"
        assert isinstance(id_, int), f"'{id_}' should be an integer"
